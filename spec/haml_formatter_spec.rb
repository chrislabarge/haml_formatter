# frozen_string_literal: true

RSpec.describe HamlFormatter do
  it "has a version number" do
    expect(HamlFormatter::VERSION).not_to be nil
  end

  it "runs the tests" do
    test_cases.each do |dir, desc|
      log_spec desc

      files = test_files(dir)

      format files

      expect_correct_format files
    end
  end
end

def test_cases
  {
    nested_hash_indent:
      "indents new lines to the previous lines hash key",
    conditional_spacing:
      "adds blank lines to the previous lines above conditionals",
    conditional_and_hash:
      "indents new lines to the previous lines hash key, and blank lines above conditionals",
    long_ruby_function:
      "splits long ruby/rails methods like link_to properly",
    inline_conditionals:
      "does something with inline conditionals",
  }
end

def format(files)
  subject = HamlFormatter::Formatter.new

  if files.is_a? Hash
    subject.format(files[:original], files[:actual])
  elsif files.is_a? String
    subject.format(files)
  end
end

def expect_correct_format(files)
  expect(no_difference?(files[:expected], files[:actual])).to eq true
end

def no_difference?(file1, file2)
  FileUtils.identical?(file1, file2)
end

def test_files(spec)
  {
    original: test_file_path(spec, :original),
    expected: test_file_path(spec, :expected),
    actual: formatted_test_file_path(spec, :actual),
  }
end

def test_file_path(spec, haml_file_name)
  base_path = "./spec/support/haml/#{spec}/"

  # Move this to a test dir initialzer
  init_test_dir base_path

  "#{base_path}/#{haml_file_name}.html.haml"
end

def formatted_test_file_path(spec, haml_file_name)
  base_path = "./spec/formatted/haml/#{spec}"

  # Move this to a test dir initialzer
  init_test_dir base_path

  "#{base_path}/#{haml_file_name}.html.haml"
end

def init_test_dir(path)
  Dir.mkdir(path) unless Dir.exist?(path)
end

def log_spec(description)
  puts "Running #==> #{description} spec"
end
