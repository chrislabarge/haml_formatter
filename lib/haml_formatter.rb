# frozen_string_literal: true

require "haml_formatter/version"
require "haml_formatter/splitter"
require "haml_formatter/file_helper"

module HamlFormatter
  class Error < StandardError; end

  class Formatter
    def initialize
      @adjusted_line_index = 0
      @first_char_index = 0
      @ruby_eval_index = 0
      @file_lines = nil
    end

    def format(file, new_file = nil)
      file_helper = FileHelper.new
      @file_lines = file_helper.read(file)

      process_formatting

      file_helper.write_to(new_file, @file_lines)
    end

    def process_formatting
      escaped_lines = @file_lines.map(&:chomp)

      escaped_lines.each_with_index do |line, index|
        set_first_char_index line

        # Handle the conditional spacing
        if ruby_evaluated_conditional?(line)
          insert_blank_line(index)
          @within_conditional = true
          next
        end

        if @within_conditional && @ruby_eval_index >= @first_char_index
          insert_blank_line(index)
          @within_conditional = false
        end

        split_line(line) if exceeds_max_length line
      end
    end

    def ruby_line?(line)
      ruby_evaluators = ["=", "-"]
      characters = line.chars

      characters.each_with_index do |char, index|
        next if char.empty? && index != (characters.count - 1)

        if ruby_evaluators.include? char
          @ruby_eval_index = index
          return true
        end
        break
      end

      false
    end

    def split_line(line)
      splitter = Splitter.new
      splits = splitter.split_line(line)

      splits.each_with_index do |split_line, index|
        set_indent_index(splits, index)
        write_split_line(split_line, index)

        @adjusted_line_index += 1 unless index == (splits.count - 1)
      end
    end

    def set_indent_index(splits, index)
      return unless index != 0

      previous_line = splits[index - 1]

      if (hash_index = previous_line.chars.rindex("{"))
        # this is lining up with firs symbol
        # TODO: COME UP WITH A SOLUTION TO BETTER FINE THE
        # FIRST KEY TO LINE UP WITH
        @indent_index = hash_index + 2
      end
    end

    def exceeds_max_length(line)
      line.chars.count > 79
    end

    # this can probably come from a different loop
    def set_first_char_index(line)
      line.chars.each_with_index do |char, index|
        next if char.strip.empty?

        @first_char_index = index
        return
      end
    end

    def ruby_conditional?(line)
      # these have to be better at determining if it is a conftional
      # What is the first word has if in it
      line[(@first_char_index + 1)..-1].split(" ") do |word|
        return true if word.include?("if") ||
          word.include?("elsif") ||
          word.include?("unless") ||
          word.include?("else")
      end

      false
    end

    def ruby_evaluated_conditional?(line)
      ruby_line?(line) && ruby_conditional?(line)
    end

    def insert_blank_line(index)
      @file_lines.insert(index + @adjusted_line_index, "")
      @adjusted_line_index += 1
    end

    def write_split_line(line, index)
      if index.zero?
        @file_lines[@adjusted_line_index] = line
      else
        insert_formatted_line line
      end
    end

    def insert_formatted_line(line)
      formatted_line = format_new_line(line, @indent_index)

      @file_lines.insert(@adjusted_line_index, formatted_line)
    end

    def format_new_line(line, column_index = 2)
      column_index ||= @first_char_index + 2

      blank_spaces = column_index - blank_space_prefix_count(line)

      args = Array.new(blank_spaces, " ")

      line.prepend(*args)
    end

    def blank_space_prefix_count(line)
      count = 0

      line.chars.each do |char|
        return count unless char.strip.empty?

        count += 1
      end

      count
    end
  end
end
