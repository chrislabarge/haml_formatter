# frozen_string_literal: true

module HamlFormatter
  class Validator
    def validate_file(file)
      return if file.split(".").last == "haml"

      raise StandardError, "Must be a haml file"
    end
  end
end
