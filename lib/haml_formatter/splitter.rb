# frozen_string_literal: true

module HamlFormatter
  class Splitter
    DOUBLE_QUOTE_REGEX = /,(?!(?=[^"]*"[^"]*(?:"[^"]*"[^"]*)*$))/.freeze
    QUOTE_REGEX = /,(?!(?=[^']*'[^']*(?:'[^']*'[^']*)*$))/.freeze
    PARENTHESES_REGEX = /,(?![^()]*\))/.freeze

    def split_exception_regexes
      [
        DOUBLE_QUOTE_REGEX,
        QUOTE_REGEX,
        PARENTHESES_REGEX,
      ]
    end

    def split_line(line)
      indexes = find_comma_split_index(line)

      splits = []

      old_index = -1
      indexes.each do |i|
        splits << line[old_index + 1..i].strip
        old_index = i
      end

      splits << line[indexes.last + 1..-1].strip

      splits
    end

    def find_comma_split_index(string)
      split_indexes = find_all_split_indexes(string)
      find_matching_split_indexes(split_indexes)
    end

    def find_all_split_indexes(string)
      split_exception_regexes.map do |regex|
        string.gsub(regex).map { Regexp.last_match.begin(0) }
      end
    end

    def find_matching_split_indexes(split_indexes)
      actual_indexes = nil

      split_indexes.each_with_index do |indexes, index|
        if index.zero?
          actual_indexes = indexes
          next
        end

        actual_indexes &= indexes
      end

      actual_indexes
    end
  end
end
