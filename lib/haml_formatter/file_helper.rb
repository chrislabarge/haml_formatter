# frozen_string_literal: true

require "haml_formatter/validations"

module HamlFormatter
  class FileHelper
    def read(file)
      validator = Validator.new
      validator.validate_file(file)

      file = File.open(file)

      file.readlines
    end

    def write_to(new_file, lines)
      File.open(new_file, "w+") do |f|
        lines.each { |element| f.puts(element) }
      end
    end
  end
end
